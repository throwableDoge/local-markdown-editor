package cn.throwx.md;

import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


/**
 * @author throwable
 * @version v1
 * @description
 * @since 2021/8/14 19:56
 */
public class JavaFxMarkdownEditor extends Application {

    private static final int W = 1200;
    private static final int H = 1000;
    private static final String TITLE = "markdown editor";

    public static String CONTENT = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\"/>\n" +
            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" +
            "    <title>ByteMD example</title>\n" +
            "    <link rel=\"stylesheet\" href=\"https://unpkg.com/bytemd/dist/index.min.css\"/>\n" +
            "    <link rel=\"stylesheet\" href=\"https://unpkg.com/github-markdown-css\"/>\n" +
            "    <script src=\"https://unpkg.com/bytemd\"></script>\n" +
            "    <script src=\"https://unpkg.com/@bytemd/plugin-gfm\"></script>\n" +
            "    <script src=\"https://unpkg.com/@bytemd/plugin-highlight\"></script>\n" +
            "    <style>\n" +
            "        .bytemd {\n" +
            "            height: calc(100vh - 50px);\n" +
            "        }\n" +
            "\n" +
            "        .footer {\n" +
            "            width: 100%;\n" +
            "            height: 30px;\n" +
            "            left: 0;\n" +
            "            position: absolute;\n" +
            "            bottom: 0;\n" +
            "            text-align: center;\n" +
            "        }\n" +
            "    </style>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div class=\"footer\">\n" +
            "    <a href=\"https://github.com/bytedance/bytemd\">bytemd</a>\n" +
            "</div>\n" +
            "<script>\n" +
            "    const plugins = [bytemdPluginGfm(), bytemdPluginHighlight()];\n" +
            "    const editor = new bytemd.Editor({\n" +
            "        target: document.body,\n" +
            "        props: {\n" +
            "            value: '# heading\\n\\nparagraph\\n\\n> blockquote',\n" +
            "            plugins,\n" +
            "        },\n" +
            "    });\n" +
            "    editor.$on('change', (e) => {\n" +
            "        editor.$set({value: e.detail.value});\n" +
            "    });\n" +
            "</script>\n" +
            "</body>\n" +
            "</html>";


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setWidth(W);
        stage.setHeight(H);
        Scene scene = new Scene(new Group());
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        webEngine.getLoadWorker().stateProperty()
                .addListener((ov, oldState, newState) -> {
                    if (newState == Worker.State.SUCCEEDED) {
                        stage.setTitle(TITLE);
                    }
                });
        webEngine.loadContent(CONTENT);
        scene.setRoot(browser);
        stage.setScene(scene);
        stage.show();
    }
}
