package cn.throwx.md;

import com.formdev.flatlaf.FlatIntelliJLaf;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javax.swing.*;
import java.awt.*;


/**
 * @author throwable
 * @version v1
 * @description
 * @since 2021/8/14 19:56
 */
public class MarkdownEditor {

    private static final int W = 1200;
    private static final int H = 1000;
    private static final String TITLE = "markdown editor";

    public static String CONTENT = "<!DOCTYPE html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\"/>\n" +
            "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n" +
            "    <title>ByteMD example</title>\n" +
            "    <link rel=\"stylesheet\" href=\"https://unpkg.com/bytemd/dist/index.min.css\"/>\n" +
            "    <link rel=\"stylesheet\" href=\"https://unpkg.com/github-markdown-css\"/>\n" +
            "    <script src=\"https://unpkg.com/bytemd\"></script>\n" +
            "    <script src=\"https://unpkg.com/@bytemd/plugin-gfm\"></script>\n" +
            "    <script src=\"https://unpkg.com/@bytemd/plugin-highlight\"></script>\n" +
            "    <style>\n" +
            "        .bytemd {\n" +
            "            height: calc(100vh - 50px);\n" +
            "        }\n" +
            "\n" +
            "        .footer {\n" +
            "            width: 100%;\n" +
            "            height: 30px;\n" +
            "            left: 0;\n" +
            "            position: absolute;\n" +
            "            bottom: 0;\n" +
            "            text-align: center;\n" +
            "        }\n" +
            "    </style>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div class=\"footer\">\n" +
            "    <a href=\"https://github.com/bytedance/bytemd\">bytemd</a>\n" +
            "</div>\n" +
            "<script>\n" +
            "    const plugins = [bytemdPluginGfm(), bytemdPluginHighlight()];\n" +
            "    const editor = new bytemd.Editor({\n" +
            "        target: document.body,\n" +
            "        props: {\n" +
            "            value: '# heading\\n\\nparagraph\\n\\n> blockquote',\n" +
            "            plugins,\n" +
            "        },\n" +
            "    });\n" +
            "    editor.$on('change', (e) => {\n" +
            "        editor.$set({value: e.detail.value});\n" +
            "    });\n" +
            "</script>\n" +
            "</body>\n" +
            "</html>";

    static {
        // 初始化主题
        try {
            UIManager.setLookAndFeel(FlatIntelliJLaf.class.getName());
        } catch (Exception e) {
            throw new IllegalStateException("theme init error", e);
        }
    }

    private static JFrame buildFrame(int w, int h, LayoutManager layoutManager) {
        JFrame frame = new JFrame();
        frame.setLayout(layoutManager);
        frame.setTitle(TITLE);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(w, h);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int x = (int) (toolkit.getScreenSize().getWidth() - frame.getWidth()) / 2;
        int y = (int) (toolkit.getScreenSize().getHeight() - frame.getHeight()) / 2;
        frame.setLocation(x, y);
        return frame;
    }

    private static void initAndDisplay() {
        // 构建窗体
        JFrame frame = buildFrame(W, H, new BorderLayout());
        JFXPanel panel = new JFXPanel();
        Platform.runLater(() -> {
            panel.setSize(W, H);
            initWebView(panel, CONTENT);
            frame.getContentPane().add(panel);
        });
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(MarkdownEditor::initAndDisplay);
    }

    private static void initWebView(JFXPanel fxPanel, String content) {
        StackPane root = new StackPane();
        Scene scene = new Scene(root);
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.setJavaScriptEnabled(true);
        webEngine.loadContent(content);
        root.getChildren().add(webView);
        fxPanel.setScene(scene);
    }
}
