module cn.throwx.md {
    requires javafx.controls;
    requires javafx.web;
    requires java.desktop;
    requires com.formdev.flatlaf;
    requires javafx.swing;

    opens cn.throwx.md to javafx.graphics;
}